.. D3M Interface documentation master file, created by
   sphinx-quickstart on Wed Jul  8 10:30:30 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to D3M Interface's documentation!
=========================================

D3M Interface is a python library which enable data scientics to use D3M AutoML Systems. It contains an implementation to integrate D3M TA2 systems with Jupyter Notebooks using the TA3-TA2 API. It provides a familiar interface, that is similar to other AutoML systems likes H2O and TPOT, to make easier for people to adopt D3M tools.

D3M Interface supports six TA2 systems: NYU, CMU, SRI, TAMU, and UBC.
 

You can find the source code on GitLab: https://gitlab.com/ViDA-NYU/d3m/d3m_interface

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   installation
   getting-started
   api



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
