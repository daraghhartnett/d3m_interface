## Table of contents

1. [What is in this repository](#what-is-in-this-repository)
2. [Installation](#installation)
3. [Run the JIDO Search](#run-the-jido-search)
4. [How to update](#how-to-update)


## What is in this repository
This repository holds the SRI fork of the NYU D3M Interface. The original repo can be viewed at
[https://gitlab.com/ViDA-NYU/d3m/d3m_interface/-/tree/master/](https://gitlab.com/ViDA-NYU/d3m/d3m_interface/-/tree/master/)


## Installation
To install this version of the D3M Interface do the following:
1. Before starting, it is recommended that you create a conda environment using the following commands. This will allow
you to keep the environment used to run the demo separate from your other work:

    ```conda create -n <environment name> python=3.6.9```<br>
    ```conda activate <environment name>```
    
2. Clone the repo fork:

    ```git clone https://gitlab.com/daraghhartnett/d3m_interface```
    
3. Build the binary locally:

    ```cd d3m_interface```<br>
    ```python setup.py sdist bdist_wheel```
    
4. Install the local binary (this is not on pypi just yet):

    ```pip install dist/d3m_interface-0.2.0-py3-none-any.whl```
    
5. Install jupyter lab:

    ```pip install jupyterlab```<br>
    ```pip install matplotlib```<br>
    

## Run the JIDO Search
1. Open a terminal window and cd to the d3m_interface directory.

2. Start Jupyter Lab:

    ```jupyter lab```
    
3. Open 2 browsers side by side and navigate to http://localhost:8888/lab. In the file manager, open `search_JIDO.ipynb`
(regular JIDO search) in the left hand browser and `search_JIDO_with_rules.ipynb` (search JIDO with the additional 
rules provided by Valet) in the right hand browser.

4. Run the steps in sequence by clicking the run icon in the left hand window. Here is the timing to expect once search 
starts:
    
    ```                        
                                  m  s
    Launch:                      00.00
    First scores & plot renders: 02.56
    Session complete:            04.50
    ```

5. If you would rather end the session early, click the square in the tool bar menu that has the tool hint `Interrupt 
the kernel`. This will handle the cleanup of the ta2 gracefully - look for the messages in the following step to know
when it is safe to proceed.

6. Once you see the following messages below the plot it is safe to run the other notebook in the right hand browser:

    ```
    INFO: Ending session...
    INFO: Session ended!
    ```

7. Note: Make sure you dont have the same notebook opened in both tabs or they will start sending each other updates.
Nothing bad will happen - it will just be annoying.


## How to update
1. To update the to the latest SRI TA2 Docker image, run:

    ```docker pull registry.gitlab.com/daraghhartnett/autoflow:latest```
    
2. To get the latest version of this code, run the following in the `d3m_interface` folder:

    ```git pull```<br>
    ```pip uninstall d3m_interface```<br>
    ```rm dist/*```<br>
    ```python setup.py sdist bdist_wheel```<br>
    ```pip install dist/d3m_interface-0.2.0-py3-none-any.whl```<br>


## Configuration Options
The following configuration options can be found in both `search_JIDO.ipynb` and `search_JIDO_with_rules.ipynb`
1. Font Size: In the second cell  there is a variable called 
`BIGGER_SIZE`. Use this to change the overall font sizes on the plots - these can also be more finely tuned on the 
following lines where each of the font types can be individually set.

2. Bar Colors: The alternating bar colors are set using RGB values. These can be see in the for loop in the second cell
in the variables called `color`. The last parameter to the color variable is the transparency (1.0 is fully opaque, 0.0 is
fully transparent)

3. Size of the chart: See the inline variable `figsize` in the second cell. Change both numbers to be larger or smaller
to make the plot larger or smaller respectively.

4. Path to the datasets: These are at the top of the 3rd call. Change the `dataset_path` variable to point at the dataset
you would like to operate on.



    